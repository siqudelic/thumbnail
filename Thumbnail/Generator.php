<?php


namespace Siqu\ThumbnailBundle\Thumbnail;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;


/**
 * Class Generator
 * @package Siqu\ThumbnailBundle\Thumbnail
 * @author Sebastian Paulmichl <paulmichlsebastian@gmail.com>
 * @copyright Copyright (c), Sebastian Paulmichl
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Generator
{
    /** @var CacheInterface */
    private $cache;

    /** @var string */
    private $imageDir;

    /** @var \Imagick */
    private $imageMagic;

    /** @var Finder */
    private $finder;


    /**
     * Generator constructor.
     * @param CacheInterface $cache
     * @param \Imagick $imageMagic
     * @param string $imageDir
     */
    public function __construct(
        CacheInterface $cache,
        \Imagick $imageMagic,
        string $imageDir
    )
    {
        $this->cache = $cache;
        $this->imageMagic = $imageMagic;
        $this->imageDir = $imageDir;

        $this->finder = new Finder();
        $this->finder->files()->in($this->imageDir);
    }

    /**
     * Check if image has a valid thumbnail.
     * If not create a thumbnail for the image and cache it.
     *
     * @param string $imageName
     * @param int $width
     * @param int $height
     * @return string
     */
    public function generate(
        string $imageName,
        int $width,
        int $height
    )
    {
        $origName = $imageName;
        // We have a subdirectory
        if (strpos($imageName, '/') !== false) {
            $directories = explode('/', $imageName);
            // Extract true image name
            $imageName = array_slice($directories, -1);
            $imageName = array_shift($imageName);
            // Remove file name from directories
            $directories = array_slice($directories, 0, -1);
            $directories = implode('/', $directories);

            $this->finder->files()->in($this->imageDir . '/' . $directories);
        }
        $this->finder->name($imageName);
        $iterator = $this->finder->getIterator();
        $iterator->rewind();

        /** @var SplFileInfo $file */
        $file = $iterator->current();
        // Retrieve modify time of original image
        $modifyTime = $file->getMTime();

        // Check if cached image exists
        if ($this->cache->fileExists($imageName, $width, $height, $modifyTime)) {
            $cacheFile = $this->cache->getCacheFile($imageName, $width, $height, $modifyTime);

            // Return original file if cache file does not exist or was not readable.
            if (!$cacheFile) {
                return $file->getContents();
            } else {
                return $cacheFile->getContents();
            }
        }

        $this->imageMagic->readImage($file->getRealPath());

        $this->imageMagic->optimizeImageLayers();
        $this->imageMagic->setImageCompressionQuality(60);
        $this->imageMagic->setImageFormat('jpg');
        $this->imageMagic->stripImage();
        $this->imageMagic->setCompression(\Imagick::COMPRESSION_JPEG);
        $this->imageMagic->resizeImage($width, 0, \Imagick::FILTER_LANCZOS, 0.9);

        if($height !== 0) {
	        $this->imageMagic->cropImage($width, $height,
		        ($this->imageMagic->getImageWidth() - $width) / 2,
		        ($this->imageMagic->getImageHeight() - $height) / 2
	        );
        }

        $this->cache->cacheFile($this->imageMagic, $origName, $width, $height, $modifyTime);

        return $this->imageMagic->getImageBlob();
    }
}