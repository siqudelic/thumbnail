<?php

namespace Siqu\ThumbnailBundle\Thumbnail;

use Symfony\Component\Finder\SplFileInfo;

/**
 * Interface CacheInterface
 *
 * All Cache implementations must implement this interface to allow the generator to properly cache a image.
 *
 * @package Siqu\ThumbnailBundle\Thumbnail
 * @author Sebastian Paulmichl <paulmichlsebastian@gmail.com>
 * @copyright Copyright (c), Sebastian Paulmichl
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
interface CacheInterface
{
    /**
     * Check if a cache file exists for the given image file.
     * @param string $fileName
     * @param int $width
     * @param int $height
     * @param int $modifyTime
     * @return bool
     */
    public function fileExists(string $fileName, int $width, int $height, int $modifyTime);

    /**
     * Retrieve the cache file.
     * Return null if file does not exist.
     *
     * @param string $fileName
     * @param int $width
     * @param int $height
     * @param int $modifyTime
     * @return SplFileInfo|null
     */
    public function getCacheFile(string $fileName, int $width, int $height, int $modifyTime);

    /**
     * Cache the Imagick content.
     *
     * @param \Imagick $imageMagic
     * @param string $fileName
     * @param int $width
     * @param int $height
     * @param int $modifyTime
     */
    public function cacheFile(\Imagick $imageMagic, string $fileName, int $width, int $height, int $modifyTime);
}