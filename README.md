Symfony Bundle to generate thumbnails on the fly
=======================

[![Build](https://gitlab.com/Siqu/Thumbnail/badges/master/build.svg)](https://gitlab.com/Siqu/Thumbnail/pipelines)
[![Coverage report](https://gitlab.com/Siqu/Thumbnail/badges/master/coverage.svg?job=test)](https://siqu.gitlab.io/Thumbnail/report/)

Bundle provides a controller to create thumbnails on demand.

Include the routing via:

```yaml
siqu_thumbnail:
    resource: "@SiquThumbnailBundle/Resources/config/routing.yml"
    type:     yaml
    prefix:   /{PREFIX}
```

This route will create the requested thumbnail and store the generated file inside the symfony cache.
The image name will be resolved via the config and the resulting cache file will be stored in a 
subdirectory, which can be configured via the config inside of the symfony cache directory.

The resulting thumbnail will receive a cache header with a configurable max_age.

When the original file changed it will create a new cache file.


Configuration can me made with the following settings:

```yaml
siqu_thumbnail:
    image_dir:  %kernel.root_dir%/Resources/images (default)
    cache_dir:  %kernel.cache_dir%/thumbnails (default)
    cache_time: 604800 (default 1 week)
```


Thumbnails can then be delivered via the route

```
/{PREFIX}/{width}/{height}/{imageName}
```

