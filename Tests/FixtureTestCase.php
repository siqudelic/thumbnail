<?php


namespace Siqu\ThumbnailBundle\Tests;


/**
 * Trait FixtureTestCase
 * @package Siqu\ThumbnailBundle\Tests
 * @author Sebastian Paulmichl <paulmichlsebastian@gmail.com>
 * @copyright Copyright (c), Sebastian Paulmichl
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
trait FixtureTestCase
{
    private $fixturesPath = __DIR__ . '/Fixtures';
}