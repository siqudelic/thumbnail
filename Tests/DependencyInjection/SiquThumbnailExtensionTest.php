<?php

namespace Siqu\ThumbnailBundle\Tests\DependencyInjection;

use PHPUnit\Framework\TestCase;
use Siqu\ThumbnailBundle\DependencyInjection\SiquThumbnailExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class SiquThumbnailExtensionTest
 * @package Siqu\ThumbnailBundle\Tests\DependencyInjection
 * @author Sebastian Paulmichl <paulmichlsebastian@gmail.com>
 * @copyright Copyright (c), Sebastian Paulmichl
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class SiquThumbnailExtensionTest extends TestCase
{
    /** @var ContainerBuilder|\PHPUnit_Framework_MockObject_MockObject */
    private $container;

    /** @var  SiquThumbnailExtension */
    private $extension;

    /**
     * Should set container parameter
     */
    public function testLoad()
    {
        $this->container->expects($this->exactly(3))
            ->method('setParameter')
            ->withConsecutive(
                [
                    'siqu_thumbnail.image_dir',
                    'rootDir/Resources/images'
                ],
                [
                    'siqu_thumbnail.cache_dir',
                    'cacheDir/thumbnails'
                ],
                [
                    'siqu_thumbnail.cache_time',
                    604800
                ]
            );

        $this->extension->load([], $this->container);
    }

    /**
     * Create extension for all tests
     */
    protected function setUp()
    {
        parent::setUp();

        $this->container = $this->getMockBuilder(ContainerBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->container->expects($this->exactly(2))
            ->method('getParameter')
            ->willReturnOnConsecutiveCalls('rootDir', 'cacheDir');

        $this->extension = new SiquThumbnailExtension();
    }
}
