<?php


namespace Siqu\ThumbnailBundle\Tests\Controller;

use Siqu\ThumbnailBundle\Tests\FixtureTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


/**
 * Class ThumbnailControllerTest
 * @package Siqu\ThumbnailBundle\Tests\Controller
 * @author Sebastian Paulmichl <paulmichlsebastian@gmail.com>
 * @copyright Copyright (c), Sebastian Paulmichl
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class ThumbnailControllerTest extends WebTestCase
{
    use FixtureTestCase;

    /** @var Client */
    private $client;

    /**
     * Should return image as response
     */
    public function testGenerateAction()
    {
        $this->client->request('GET', '/thumbnail/100/100/test.jpg');

        $response = $this->client->getResponse();

        $this->assertEquals('image/jpeg', $response->headers->get('Content-type'));
        $this->assertEquals('inline; filename="test.jpg"', $response->headers->get('Content-Disposition'));
        $this->assertEquals(604800, $response->getMaxAge());
    }

    /**
     * Should return image as response
     */
    public function testGenerateActionWithZeroHeight()
    {
        $this->client->request('GET', '/thumbnail/50/0/test.jpg');

        $response = $this->client->getResponse();

        $this->assertEquals('image/jpeg', $response->headers->get('Content-type'));
        $this->assertEquals('inline; filename="test.jpg"', $response->headers->get('Content-Disposition'));
        $this->assertEquals(604800, $response->getMaxAge());
    }

    /**
     * Should return image from subdirectory
     */
    public function testGenerateActionSubdirectory()
    {
        $this->client->request('GET', '/thumbnail/100/100/sub/test.jpg');

        $response = $this->client->getResponse();

        $this->assertEquals('image/jpeg', $response->headers->get('Content-type'));
        $this->assertEquals('inline; filename="sub/test.jpg"', $response->headers->get('Content-Disposition'));
        $this->assertEquals(604800, $response->getMaxAge());
    }

    protected function setUp()
    {
        parent::setUp();

        $this->client = static::createClient();
    }
}