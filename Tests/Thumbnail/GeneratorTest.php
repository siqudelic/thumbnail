<?php

namespace Siqu\ThumbnailBundle\Tests\Thumbnail;

use PHPUnit\Framework\TestCase;
use Siqu\ThumbnailBundle\Tests\FixtureTestCase;
use Siqu\ThumbnailBundle\Thumbnail\CacheInterface;
use Siqu\ThumbnailBundle\Thumbnail\Generator;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class GeneratorTest
 * @package Siqu\ThumbnailBundle\Tests\Thumbnail
 * @author Sebastian Paulmichl <paulmichlsebastian@gmail.com>
 * @copyright Copyright (c), Sebastian Paulmichl
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class GeneratorTest extends TestCase
{
    use FixtureTestCase;

    /** @var CacheInterface|\PHPUnit_Framework_MockObject_MockObject */
    private $cache;

    /** @var \Imagick|\PHPUnit_Framework_MockObject_MockObject */
    private $imageMagic;

    /** @var Generator */
    private $generator;

    /** @var string */
    private $imagePath;

    /**
     * Should not create new image.
     */
    public function testGenerateCacheExisting()
    {
        // should not be called
        $this->imageMagic->expects($this->never())
            ->method('getImageBlob');

        $this->cache->expects($this->once())
            ->method('fileExists')
            ->willReturn(true);
        $this->cache->expects($this->once())
            ->method('getCacheFile')
            ->willReturn(new SplFileInfo($this->fixturesPath . '/cacheDir/19b53a59.jpg', '', ''));

        $this->assertEquals(file_get_contents($this->fixturesPath . '/cacheDir/19b53a59.jpg'), $this->generator->generate('test.jpg', 0, 0));
    }

    /**
     * Should return original content
     */
    public function testGenerateCacheReturnsNull()
    {
        // should not be called
        $this->imageMagic->expects($this->never())
            ->method('getImageBlob');

        $this->cache->expects($this->once())
            ->method('fileExists')
            ->willReturn(true);
        $this->cache->expects($this->once())
            ->method('getCacheFile')
            ->willReturn(null);

        $this->assertEquals(file_get_contents($this->imagePath . '/test.jpg'), $this->generator->generate('test.jpg', 0, 0));
    }

    /**
     * Should create thumbnail and cache content
     */
    public function testGenerateWithoutCache()
    {
        $width = 100;
        $height = 50;
        $imageName = 'test.jpg';

        $this->cache->expects($this->once())
            ->method('fileExists')
            ->willReturn(false);

        $this->imageMagic->expects($this->once())
            ->method('readImage');
        $this->imageMagic->expects($this->once())
            ->method('optimizeImageLayers');
        $this->imageMagic->expects($this->once())
            ->method('setImageCompressionQuality')
            ->with(60);
        $this->imageMagic->expects($this->once())
            ->method('setImageFormat')
            ->with('jpg');
        $this->imageMagic->expects($this->once())
            ->method('stripImage');
        $this->imageMagic->expects($this->once())
            ->method('setCompression')
            ->with(\Imagick::COMPRESSION_JPEG);
        $this->imageMagic->expects($this->once())
            ->method('resizeImage')
            ->with($width, 0, \Imagick::FILTER_LANCZOS, 0.9);
        $this->imageMagic->expects($this->once())
            ->method('cropImage')
            ->with($width, $height, 50, 25);
        $this->imageMagic->expects($this->once())
            ->method('getImageWidth')
            ->willReturn(200);
        $this->imageMagic->expects($this->once())
            ->method('getImageHeight')
            ->willReturn(100);
        $this->imageMagic->expects($this->once())
            ->method('getImageBlob')
            ->willReturn('content');

        $this->cache->expects($this->once())
            ->method('cacheFile')
            ->with($this->imageMagic, $imageName, $width, $height, $this->isType('integer'));

        $this->assertEquals('content', $this->generator->generate($imageName, $width, $height));
    }

    /**
     * Should create thumbnail and cache content
     */
    public function testGenerateWithoutCacheAndZeroHeight()
    {
        $width = 100;
        $height = 0;
        $imageName = 'test.jpg';

        $this->cache->expects($this->once())
            ->method('fileExists')
            ->willReturn(false);

        $this->imageMagic->expects($this->once())
            ->method('readImage');
        $this->imageMagic->expects($this->once())
            ->method('optimizeImageLayers');
        $this->imageMagic->expects($this->once())
            ->method('setImageCompressionQuality')
            ->with(60);
        $this->imageMagic->expects($this->once())
            ->method('setImageFormat')
            ->with('jpg');
        $this->imageMagic->expects($this->once())
            ->method('stripImage');
        $this->imageMagic->expects($this->once())
            ->method('setCompression')
            ->with(\Imagick::COMPRESSION_JPEG);
        $this->imageMagic->expects($this->once())
            ->method('resizeImage')
            ->with($width, 0, \Imagick::FILTER_LANCZOS, 0.9);
        $this->imageMagic->expects($this->never())
            ->method('cropImage');
        $this->imageMagic->expects($this->never())
            ->method('getImageWidth')
            ->willReturn(200);
        $this->imageMagic->expects($this->never())
            ->method('getImageHeight')
            ->willReturn(100);
        $this->imageMagic->expects($this->once())
            ->method('getImageBlob')
            ->willReturn('content');

        $this->cache->expects($this->once())
            ->method('cacheFile')
            ->with($this->imageMagic, $imageName, $width, $height, $this->isType('integer'));

        $this->assertEquals('content', $this->generator->generate($imageName, $width, $height));
    }

    /**
     * Create service for all tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->imagePath = $this->fixturesPath . '/images';

        $this->cache = $this->getMockBuilder(CacheInterface::class)
            ->getMock();

        $this->imageMagic = $this->getMockBuilder(\Imagick::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->generator = new Generator(
            $this->cache,
            $this->imageMagic,
            $this->imagePath
        );
    }
}
