<?php

namespace Siqu\ThumbnailBundle\Tests\Thumbnail;

use PHPUnit\Framework\TestCase;
use Siqu\ThumbnailBundle\Tests\FixtureTestCase;
use Siqu\ThumbnailBundle\Thumbnail\Cache;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class CacheTest
 * @package Siqu\ThumbnailBundle\Tests\Thumbnail
 * @author Sebastian Paulmichl <paulmichlsebastian@gmail.com>
 * @copyright Copyright (c), Sebastian Paulmichl
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class CacheTest extends TestCase
{
    use FixtureTestCase;

    /** @var string */
    private $cacheDir;

    /**
     * Test if thumbnail cache directory is created.
     */
    public function testConstructShouldCreateCacheDirectoryIfNotExists()
    {
        $testCacheDirectoryName = $this->fixturesPath . '/testCacheDir';

        $this->assertFalse(file_exists($testCacheDirectoryName));

        new Cache($testCacheDirectoryName, 0);

        $this->assertTrue(file_exists($testCacheDirectoryName));

        // Remove test cacheDir
        rmdir($testCacheDirectoryName);
    }

    /**
     * Should return false if file does not exist.
     */
    public function testFileExistsNonExisting()
    {
        $cache = $this->getService();
        $this->assertFalse($cache->fileExists('nonExisting', 0, 0, 0));
    }

    /**
     * @return Cache
     */
    private function getService(): Cache
    {
        $cache = new Cache($this->cacheDir, 0);
        return $cache;
    }

    /**
     * Should return true if file exist.
     */
    public function testFileExistsExisting()
    {
        $cache = $this->getService();

        $this->assertTrue($cache->fileExists('existing', 0, 0, 0));
    }

    /**
     * Should return null
     */
    public function testGetCacheFileNonExisting()
    {
        $cache = $this->getService();

        $this->assertNull($cache->getCacheFile('nonExisting', 0, 0, 0));
    }

    /**
     * Should return file.
     */
    public function testGetCacheFile()
    {
        $cache = $this->getService();

        $file = $cache->getCacheFile('existing', 0, 0, 0);

        $this->assertNotNull($file);
        $this->assertEquals(SplFileInfo::class, get_class($file));
    }

    /**
     * Should create cache file in cache directory
     */
    public function testCacheFile()
    {
        $cache = $this->getService();

        $newFilePath = $this->cacheDir . '/eda11aaf.jpg';

        $this->assertFalse(file_exists($newFilePath));

        $imageMagic = new \Imagick($this->fixturesPath . '/images/test.jpg');

        $cache->cacheFile($imageMagic, 'new', 0, 0, 0);

        $this->assertTrue(file_exists($newFilePath));

        unlink($newFilePath);
    }

    /**
     * Set cache director path.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->cacheDir = $this->fixturesPath . '/cacheDir';
    }
}
