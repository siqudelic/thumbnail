<?php

namespace Siqu\ThumbnailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ThumbnailController
 * @package Siqu\ThumbnailBundle\Controller
 * @author Sebastian Paulmichl <paulmichlsebastian@gmail.com>
 * @copyright Copyright (c), Sebastian Paulmichl
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class ThumbnailController extends Controller
{
    /**
     * Generate a thumbnail for the given file.
     *
     * @param int $width
     * @param int $height
     * @param string $file
     * @return Response
     */
    public function generateAction(
        int $width,
        int $height,
        string $file
    )
    {
        $generator = $this->get('siqu_thumbnail.generator');
        $image = $generator->generate($file, $width, $height);

        $response = new Response($image);
        $response->headers->set('Content-type', 'image/jpeg');
        $response->headers->set('Content-Disposition', 'inline; filename="' . $file . '"');
        $response->setPublic();
        $response->setMaxAge($this->getParameter('siqu_thumbnail.cache_time'));

        return $response;
    }
}