<?php

namespace Siqu\ThumbnailBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * Process the configuration for thumbnail bundle.
 *
 * @package Siqu\ThumbnailBundle\DependencyInjection
 * @author Sebastian Paulmichl <paulmichlsebastian@gmail.com>
 * @copyright Copyright (c), Sebastian Paulmichl
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Configuration implements ConfigurationInterface
{
    /** @var string */
    private $rootDir;

    /** @var string */
    private $cacheDir;

    /**
     * Configuration constructor.
     * @param string $rootDir
     * @param string $cacheDir
     */
    public function __construct(
        string $rootDir,
        string $cacheDir
    )
    {
        $this->rootDir = $rootDir;
        $this->cacheDir = $cacheDir;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('siqu_thumbnail');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        //@formatter:off;
        $rootNode
            ->children()
                ->scalarNode('image_dir')->defaultValue($this->rootDir . '/Resources/images')->end()
                ->scalarNode('cache_dir')->defaultValue($this->cacheDir . '/thumbnails')->end()
                ->integerNode('cache_time')->defaultValue(604800)->end()
            ->end();
        //@formatter:on;
        return $treeBuilder;
    }
}
